# weather_application_apixu_api

This is a simple weather application using apixu weather API (https://www.apixu.com) that shows current temperature along with the temperature forecast for the next 4 days (average temperature for each day should be shown for the forecast)